.PHONY: test clean googletest ci-init

CXXFLAGS := -pipe -std=c++17 -Wall -g -O0 -MMD -fprofile-arcs -ftest-coverage -Igoogletest/googletest/include

RMF = rm -rf --

test: test_runner
	./test_runner --gtest_output=xml
ifneq ($(CI),1)
	gcovr --output coverage.html --html --html-details --html-details-syntax-highlighting -r .
	gcovr --output coverage.xml --cobertura-pretty -r .
else
	gcovr --output coverage.xml --xml -r .
endif

clean:
	$(RMF) test_runner* *.oxx *.d .cache/ test_detail.* coverage.*

test_runner: tests.oxx
	$(CXX) $(CXXFLAGS) -o $@ $< ./googletest/build/lib/libgtest.a ./googletest/build/lib/libgtest_main.a -lpthread

%.oxx: %.cpp
	$(CXX) $(CXXFLAGS) -c -o $@ $<

-include $(wildcard *.d)

googletest:
	mkdir -p googletest/build
	cd googletest/build && cmake ..
	cd googletest/build && cmake .. -DBUILD_GMOCK=OFF
	cd googletest/build && make -j
	#cd googletest/build && make install

ci-init:
	apt update -qqy
	apt install -qqy --no-install-recommends git cmake g++ gcovr ca-certificates
	git submodule update --init
	make googletest
