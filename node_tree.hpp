#pragma once
#include <string>
#include <memory>
#include <vector>

/*
 * Nodes in net specification
 *
 * See node_path for the grammar spec
 */

struct TerminalNode;
struct StructNode;
struct ArrayNode;

struct NodeVisitor
{
	virtual void visit(const TerminalNode& node) = 0;
	virtual void visit(const StructNode& node) = 0;
	virtual void visit(const ArrayNode& node) = 0;
};

struct Node
{
	std::string name;
	bool invert { false };

	virtual void accept(NodeVisitor *visitor) = 0;
};

using NodePtr = std::unique_ptr<Node>;

struct TerminalNode : Node
{
	virtual void accept(NodeVisitor *visitor)
	{
		visitor->visit(*this);
	}
};

struct StructNode : Node
{
	std::vector<std::unique_ptr<Node>> members;

	virtual void accept(NodeVisitor *visitor)
	{
		visitor->visit(*this);
	}
};

struct ArrayNode : Node
{
	unsigned min_index;
	unsigned max_index;

	virtual void accept(NodeVisitor *visitor)
	{
		visitor->visit(*this);
	}
};
