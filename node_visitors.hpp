#pragma once
#include <string>
#include <vector>
#include "node_tree.hpp"

/* Visitor to build specification string from tree */
struct PathSpecVisitor : NodeVisitor
{
	std::string result;

	void emit_name(const Node& node)
	{
		if (node.invert) {
			result += "~{";
		}
		result += node.name;
		if (node.invert) {
			result += "}";
		}
	}

	virtual void visit(const TerminalNode& node)
	{
		emit_name(node);
	}

	virtual void visit(const StructNode& node)
	{
		emit_name(node);
		result += '{';
		bool first = true;
		for (const auto& member : node.members) {
			if (first) {
				first = false;
			} else {
				result += ' ';
			}
			member->accept(this);
		}
		result += '}';
	}

	virtual void visit(const ArrayNode& node)
	{
		emit_name(node);
		result += '[';
		result += std::to_string(node.min_index);
		result += "..";
		result += std::to_string(node.max_index);
		result += ']';
	}
};

/* Visitor to generate list of all nets specified by tree */
struct NetNameVisitor : NodeVisitor
{
	std::vector<std::string> result;
	std::vector<std::string> prefix { "" };

	std::string decorate_expr(const Node& node, const std::string& expression)
	{
		if (node.invert) {
			return "~{" + expression + "}";
		}
		return expression;
	}

	virtual void visit(const TerminalNode& node)
	{
		result.emplace_back(prefix.back() + decorate_expr(node, node.name));
	}

	virtual void visit(const StructNode& node)
	{
		prefix.emplace_back(prefix.back() + decorate_expr(node, node.name) + ".");
		for (const auto& member : node.members) {
			member->accept(this);
		}
		prefix.pop_back();
	}

	virtual void visit(const ArrayNode& node)
	{
		for (auto index = node.min_index; index <= node.max_index; ++index) {
			result.emplace_back(prefix.back() + decorate_expr(node, node.name + std::to_string(index)));
		}
	}
};
