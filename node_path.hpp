#pragma once
#include <string>
#include <memory>
#include <vector>
#include <stdexcept>
#include "node_tree.hpp"
#include "node_visitors.hpp"

/*
 * Grammar:
 * Root: Node <Terminal>
 * Node: <Name> ( | <Array> | <Struct> )
 * Name: <PermittedNameChar>+
 * Array: "[" <Number> .. <Number> "]"
 * Struct: "{" <Node> ( <Node> )* "}"
 */

/* Parse tree */
struct PathParseTree
{
	std::unique_ptr<Node> root;

	/* Generate specification string */
	auto generate_spec()
	{
		PathSpecVisitor visitor;
		root->accept(&visitor);
		return std::move(visitor.result);
	}

	/* Generate list of all net names */
	auto generate_net_names()
	{
		NetNameVisitor visitor;
		root->accept(&visitor);
		return std::move(visitor.result);
	}
};

/* Exception for failed parse, includes clang-style caret context */
struct parse_error_with_caret :
	std::runtime_error
{
	static std::string generate_caret_message(
		const std::string& message,
		const std::string& expression,
		unsigned position
	)
	{
		static const auto newline = "\n";
		static const auto pad_left = "    ";
		std::string result;
		result = message + newline;
		result += pad_left + expression + newline;
		result += pad_left + std::string(position, '-') + '^' + newline;
		return result;
	}

	parse_error_with_caret(
		const std::string& message,
		const std::string& expression,
		unsigned position
	) :
		std::runtime_error(generate_caret_message(message, expression, position))
	{
	}
};

/* The parser */
class SpecParser
{
	std::string spec;
	unsigned position { 0 };

	bool next_is_terminal() const
	{
		return position == spec.size();
	}

	bool next_is_space() const
	{
		return peek_char() == ' ';
	}

	static bool is_digit(char ch)
	{
		return isdigit(ch);
	}

	static bool is_name_char(char ch)
	{
		return isalnum(ch) || ch == '_' || ch == '-' || ch == '+' || ch == '.';
	}

	char peek_char() const
	{
		if (next_is_terminal()) {
			throw parse_error_with_caret("Unexpected end of expression", spec, position);
		}
		return spec[position];
	}

	void consume_char()
	{
		if (next_is_terminal()) {
			throw parse_error_with_caret("Unexpected end of expression", spec, position);
		}
		position++;
	}

	void assert_and_consume_char(char ch)
	{
		if (peek_char() != ch) {
			throw parse_error_with_caret(std::string("Expected: ") + ch, spec, position);
		}
		consume_char();
	}

	void consume_space()
	{
		while (!next_is_terminal() && next_is_space()) {
			consume_char();
		}
	}

	std::string consume_token(bool (*is_valid_char)(char))
	{
		std::string result;
		do {
			const auto ch = peek_char();
			if (is_valid_char(ch)) {
				result += ch;
				consume_char();
			} else if (result.empty()) {
				throw parse_error_with_caret("Invalid character", spec, position);
			} else {
				break;
			}
		} while (!next_is_terminal());
		return result;
	}

	static unsigned convert_string_to_index(const std::string& str)
	{
		/* No overflow check */
		unsigned result = 0;
		for (const auto ch : str) {
			result *= 10;
			result += ch - '0';
		}
		return result;
	}

	void parse_array(ArrayNode& node)
	{
		assert_and_consume_char('[');
		consume_space();
		std::string min_str = consume_token(is_digit);
		consume_space();
		assert_and_consume_char('.');
		assert_and_consume_char('.');
		consume_space();
		std::string max_str = consume_token(is_digit);
		consume_space();
		assert_and_consume_char(']');
		node.min_index = convert_string_to_index(min_str);
		node.max_index = convert_string_to_index(max_str);
	}

	void parse_struct(StructNode& node)
	{
		assert_and_consume_char('{');
		consume_space();
		while (peek_char() != '}') {
			node.members.emplace_back(parse_node());
			consume_space();
		}
		assert_and_consume_char('}');
	}

	std::string consume_name()
	{
		return consume_token(is_name_char);
	}

	NodePtr parse_node()
	{
		consume_space();
		bool invert = false;
		if (peek_char() == '~') {
			consume_char();
			assert_and_consume_char('{');
			invert = true;
		}
		std::string name = consume_name();
		if (invert) {
			assert_and_consume_char('}');
		}
		consume_space();
		if (!next_is_terminal() && peek_char() == '[') {
			ArrayNode node;
			node.name = name;
			node.invert = invert;
			parse_array(node);
			consume_space();
			return std::make_unique<ArrayNode>(std::move(node));
		} else if (!next_is_terminal() && peek_char() == '{') {
			StructNode node;
			node.name = name;
			node.invert = invert;
			parse_struct(node);
			consume_space();
			return std::make_unique<StructNode>(std::move(node));
		} else {
			TerminalNode node;
			node.name = name;
			node.invert = invert;
			return std::make_unique<TerminalNode>(std::move(node));
		}
	}

	NodePtr parse_root()
	{
		NodePtr result = parse_node();
		if (!next_is_terminal()) {
			throw parse_error_with_caret("Expected: end of expression", spec, position);
		}
		return result;
	}

public:

	PathParseTree parse(const std::string& spec_expr)
	{
		spec = spec_expr;
		position = 0;
		PathParseTree path;
		path.root = parse_root();
		return path;
	}

};
