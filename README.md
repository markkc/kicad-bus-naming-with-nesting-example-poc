Example parser for Kicad bus names, which can handle nested buses and 'arrays'.

See `tests.cpp` for examples of the bus names that can be parsed and/or rendered.

See the start of `node_path.hpp` for a brief and unacademic grammar spec.

Run `make googletest` to build googletest first.

Then run `make` to compile and test the demo.
