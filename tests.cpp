#include "node.hpp"

#include <gtest/gtest.h>

namespace structured_naming
{

/* Basic tests for name generation from tree */

TEST(PathVisitorGenerators, Terminal)
{
	PathParseTree tree;
	TerminalNode node;
	node.name = "Terminal";
	tree.root = std::make_unique<decltype(node)>(std::move(node));
	EXPECT_EQ(tree.generate_spec(), "Terminal");
	const auto nets = tree.generate_net_names();
	EXPECT_EQ(nets.size(), 1U);
	EXPECT_EQ(nets[0], "Terminal");
}

TEST(PathVisitorGenerators, TerminalInvert)
{
	PathParseTree tree;
	TerminalNode node;
	node.name = "Terminal";
	node.invert = true;
	tree.root = std::make_unique<decltype(node)>(std::move(node));
	EXPECT_EQ(tree.generate_spec(), "~{Terminal}");
	const auto nets = tree.generate_net_names();
	EXPECT_EQ(nets.size(), 1U);
	EXPECT_EQ(nets[0], "~{Terminal}");
}

TEST(PathVisitorGenerators, Array)
{
	PathParseTree tree;
	ArrayNode node;
	node.name = "Array";
	node.min_index = 8;
	node.max_index = 11;
	tree.root = std::make_unique<decltype(node)>(std::move(node));
	EXPECT_EQ(tree.generate_spec(), "Array[8..11]");
	const auto nets = tree.generate_net_names();
	EXPECT_EQ(nets.size(), 4U);
	EXPECT_EQ(nets[0], "Array8");
	EXPECT_EQ(nets[1], "Array9");
	EXPECT_EQ(nets[2], "Array10");
	EXPECT_EQ(nets[3], "Array11");
}

TEST(PathVisitorGenerators, ArrayInvert)
{
	PathParseTree tree;
	ArrayNode node;
	node.name = "Array";
	node.invert = true;
	node.min_index = 8;
	node.max_index = 11;
	tree.root = std::make_unique<decltype(node)>(std::move(node));
	EXPECT_EQ(tree.generate_spec(), "~{Array}[8..11]");
	const auto nets = tree.generate_net_names();
	EXPECT_EQ(nets.size(), 4U);
	EXPECT_EQ(nets[0], "~{Array8}");
	EXPECT_EQ(nets[1], "~{Array9}");
	EXPECT_EQ(nets[2], "~{Array10}");
	EXPECT_EQ(nets[3], "~{Array11}");
}

TEST(PathVisitorGenerators, Struct)
{
	PathParseTree tree;
	StructNode node;
	node.name = "Struct";
	{
		TerminalNode child;
		child.name = "CLK";
		node.members.push_back(std::make_unique<decltype(child)>(child));
		child.name = "MISO";
		node.members.push_back(std::make_unique<decltype(child)>(child));
		child.name = "MOSI";
		node.members.push_back(std::make_unique<decltype(child)>(child));
		child.name = "CS";
		child.invert = true;
		node.members.push_back(std::make_unique<decltype(child)>(child));
	}
	tree.root = std::make_unique<decltype(node)>(std::move(node));
	EXPECT_EQ(tree.generate_spec(), "Struct{CLK MISO MOSI ~{CS}}");
	const auto nets = tree.generate_net_names();
	EXPECT_EQ(nets.size(), 4U);
	EXPECT_EQ(nets[0], "Struct.CLK");
	EXPECT_EQ(nets[1], "Struct.MISO");
	EXPECT_EQ(nets[2], "Struct.MOSI");
	EXPECT_EQ(nets[3], "Struct.~{CS}");
}

TEST(PathVisitorGenerators, Complex)
{
	PathParseTree tree;
	StructNode node;
	node.name = "Struct";
	{
		TerminalNode child;
		child.name = "CLK";
		node.members.push_back(std::make_unique<decltype(child)>(child));
		child.name = "MISO";
		node.members.push_back(std::make_unique<decltype(child)>(child));
		child.name = "MOSI";
		node.members.push_back(std::make_unique<decltype(child)>(child));
		child.name = "CS";
		child.invert = true;
		node.members.push_back(std::make_unique<decltype(child)>(child));
	}
	tree.root = std::make_unique<decltype(node)>(std::move(node));
	EXPECT_EQ(tree.generate_spec(), "Struct{CLK MISO MOSI ~{CS}}");
	const auto nets = tree.generate_net_names();
	EXPECT_EQ(nets.size(), 4U);
	EXPECT_EQ(nets[0], "Struct.CLK");
	EXPECT_EQ(nets[1], "Struct.MISO");
	EXPECT_EQ(nets[2], "Struct.MOSI");
	EXPECT_EQ(nets[3], "Struct.~{CS}");
}

/* Tests for name parsing and round-trip equality check */

TEST(SpecParser, Terminal)
{
	const auto expr = "Terminal";
	SpecParser parser;
	PathParseTree tree;
	tree = parser.parse(expr);
	auto *casted = dynamic_cast<TerminalNode *>(tree.root.get());
	EXPECT_TRUE(casted != nullptr);
	EXPECT_EQ(tree.generate_spec(), expr);
}

TEST(SpecParser, Array)
{
	const auto expr = "Array[8..11]";
	SpecParser parser;
	PathParseTree tree;
	tree = parser.parse(expr);
	auto *casted = dynamic_cast<ArrayNode *>(tree.root.get());
	EXPECT_TRUE(casted != nullptr);
	EXPECT_EQ(tree.generate_spec(), expr);
}

TEST(SpecParser, Struct)
{
	const auto expr = "Struct{CLK MISO MOSI ~{CS}}";
	SpecParser parser;
	PathParseTree tree;
	tree = parser.parse(expr);
	auto *casted = dynamic_cast<StructNode *>(tree.root.get());
	EXPECT_TRUE(casted != nullptr);
	EXPECT_EQ(tree.generate_spec(), expr);
	EXPECT_TRUE(casted->members[3]->invert);
}

TEST(SpecParser, Mixed)
{
	const auto expr = "ULPI{CLK DIR STP NEXT D[0..7]}";
	SpecParser parser;
	PathParseTree tree;
	tree = parser.parse(expr);
	auto *casted = dynamic_cast<StructNode *>(tree.root.get());
	EXPECT_TRUE(casted != nullptr);
	EXPECT_EQ(tree.generate_spec(), expr);
	EXPECT_EQ(casted->members[0]->name, "CLK");
	EXPECT_EQ(casted->members[4]->name, "D");
	EXPECT_TRUE(dynamic_cast<ArrayNode *>(casted->members[4].get()) != nullptr);
}

TEST(SpecParser, Complex)
{
	const auto expr = "USB{VBUS GND TX{- +} RX{- +} CC[1..2] ULPI{CLK DIR STP NEXT D[0..7]}}";
	SpecParser parser;
	PathParseTree tree;
	tree = parser.parse(expr);
	auto *casted = dynamic_cast<StructNode *>(tree.root.get());
	EXPECT_TRUE(casted != nullptr);
	EXPECT_EQ(tree.generate_spec(), expr);
	EXPECT_TRUE(dynamic_cast<ArrayNode *>(casted->members[4].get()) != nullptr);
}

TEST(SpecParser, Whitespace)
{
	const auto inflated = "  USB  {  VBUS GND TX { - + } RX { - + } CC [ 1 .. 2 ]  ULPI { CLK DIR STP NEXT D [ 0.. 7 ] } }  ";
	const auto expr = "USB{VBUS GND TX{- +} RX{- +} CC[1..2] ULPI{CLK DIR STP NEXT D[0..7]}}";
	SpecParser parser;
	PathParseTree tree;
	tree = parser.parse(inflated);
	auto *casted = dynamic_cast<StructNode *>(tree.root.get());
	EXPECT_TRUE(casted != nullptr);
	EXPECT_EQ(tree.generate_spec(), expr);
	EXPECT_TRUE(dynamic_cast<ArrayNode *>(casted->members[4].get()) != nullptr);
}

/* TODO: Tests with invalid expressions to ensure they result in exceptions */

/* ... */

}
